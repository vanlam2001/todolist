import { arrTheme } from "../../Themes/ThemeManager"
import { ToDoListDarkTheme } from "../../Themes/ToDoListDarkTheme"

import { add_task, delete_task, done_task, change_theme, refresh_task, edit_task, update_task } from "../actions/ToDoListTypes"

const initialState = {
    themeToDoList: ToDoListDarkTheme,
    taskList: [
        { id: 'task-1', taskName: 'task 1', done: true },
        { id: 'task-2', taskName: 'task 2', done: false },
        { id: 'task-3', taskName: 'task 3', done: true },
        { id: 'task-4', taskName: 'task 4', done: false },
    ],

    taskEdit: { id: '-1', taskName: '', done: false },
}

export default (state = initialState, action) => {
    switch (action.type) {
        case add_task: {
            console.log('todo', action.newTask)
            // check arr
            if (action.newTask.taskName.trim() == '') {
                alert('Task name is required!');
                return { ...state }
            }
            // kiểm tra tồn tại

            let taskListUpdate = [...state.taskList];
            let index = taskListUpdate.findIndex(task => task.taskName === action.newTask.taskName);
            if (index !== -1) {
                alert('task name already exists !');
                return { ...state };
            }

            taskListUpdate.push(action.newTask);


            // Xử lý xong thì gán taskList mới vào taskLít
            state.taskList = [...taskListUpdate];
            return { ...state }
        }

        case change_theme: {
            let theme = arrTheme.find(theme => theme.id == action.themeId);
            if (theme) {
                console.log(theme);
                // set lại theme cho state.themeToDoList 
                state.themeToDoList = { ...theme.theme };
            }

            return { ...state };

        }

        case done_task: {
            // Click vào button check => dispatch lên action có taskId
            let taskListUpdate = [...state.taskList];

            let index = taskListUpdate.findIndex(task => task.id === action.taskId);
            // Từ task id tìm ra task đó ở vị trí nào trong arr tiến hành cập nhật lại thuộc tính done = true. Và cập nhật lại state của redux
            if (index !== -1) {
                taskListUpdate[index].done = true;
            }

            state.taskList = taskListUpdate;
            return { ...state }
        }

        case refresh_task: {
            let taskListUpdate = [...state.taskList];

            let index = taskListUpdate.findIndex(task => task.id === action.taskId);
            // Từ task id tìm ra task đó ở vị trí nào trong arr tiến hành cập nhật lại thuộc tính done = true. Và cập nhật lại state của redux
            if (index !== -1) {
                taskListUpdate[index].done = false;
            }

            state.taskList = taskListUpdate;
            return { ...state }
        }

        case delete_task: {
            let newTask = state.taskList.filter((item) => {
                return item.id != action.taskId;
            })
            return { ...state, taskList: newTask }
        }

        case edit_task: {
            return { ...state, taskEdit: action.task }
        }


        case update_task: {
            console.log(action.taskName)
            state.taskEdit = { ...state.taskEdit, taskName: action.taskName };

            let taskListUpdate = [...state.taskList];

            let index = taskListUpdate.findIndex(task => task.id === state.taskEdit.id);

            console.log(index);

            if (index !== -1) {
                taskListUpdate[index] = state.taskEdit;
            }

            state.taskList = taskListUpdate;
            state.taskEdit.id = { id: '-1', taskName: '', done: false }

            return { ...state }
        }


        default:
            return { ...state }
    }
}
