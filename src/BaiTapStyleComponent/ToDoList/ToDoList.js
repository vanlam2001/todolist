import React, { Component } from 'react'
import { Container } from '../../Container/Container'
import { Dropdown } from '../../Components/Dropdown'
import { ThemeProvider } from 'styled-components'

import { Heading1, Heading3, } from '../../Components/Heading'
import { TextField } from '../../Components/TextField'
import { Button } from '../../Components/Button'
import { Table, Tr, Th, Thead, } from '../../Components/Table'
import { connect } from 'react-redux'
import { addTaskAction, changeThemeAction, deleteTaskAction, doneTaskAction, editTaskAction, refresh, updateTask } from '../../redux/types/ToDoListActions'
import { arrTheme } from '../../Themes/ThemeManager'




class ToDoList extends Component {

    state = {
        taskName: '',
        disabled: true

    }

    renderTaskToDo = () => {
        return this.props.taskList.filter(task => !task.done).map((task, index) => {
            return <Tr key={index} >

                <Th style={{ verticalAlign: 'middle' }}>{task.taskName}</Th>

                <Th className='text-right'>
                    <Button onClick={() => {

                        this.setState({
                            disabled: false
                        }, () => {
                            this.props.dispatch(editTaskAction(task))
                        })
                        this.props.dispatch(editTaskAction(task))
                    }} className='ml-1'><i className='fa fa-edit'></i></Button>

                    <Button onClick={() => {
                        this.props.dispatch(doneTaskAction(task.id))
                    }} className='ml-1'><i className='fa fa-check'></i></Button>

                    <Button onClick={() => {
                        this.props.dispatch(deleteTaskAction(task.id))
                    }} className='ml-1'><i className='fa fa-trash'></i></Button>
                </Th>

            </Tr >
        })
    }

    renderTaskCompleted = () => {
        return this.props.taskList.filter(task => task.done).map((task, index) => {
            return <Tr key={index} >

                <Th style={{ verticalAlign: 'middle' }}>{task.taskName}</Th>
                <Th className='text-right'>
                    <Button onClick={() => {
                        this.props.dispatch(editTaskAction(task))
                    }} className='ml-1'><i className='fa fa-edit'></i></Button>


                    <Button onClick={() => {
                        this.props.dispatch(refresh(task.id))
                    }} className='ml-1'><i className='fa fa-refresh'></i></Button>

                    <Button onClick={() => {
                        this.props.dispatch(deleteTaskAction(task.id))
                    }} className='ml-1'><i className='fa fa-trash'></i></Button>


                </Th>

            </Tr>
        })
    }

    renderTheme = () => {
        return arrTheme.map((theme) => {
            return <option value={theme.id}>{theme.name}</option>
        })
    }

    // componentWillReceiveProps(newProps) {
    //     console.log('this.props', this.props);
    //     console.log('newProps', newProps)
    //     this.setState({
    //         taskName: newProps.taskEdit.taskName
    //     })
    // }


    // Lifecycle tỉnh không truy xuất
    // static getDerivedStateFromProps(newProps, currentState) {
    //     // newProps: là props mới, props cũ là this.props (không truy xuất được)
    //     // currentState: ứng với state hiện tại this.state
    //     // hoặc trả về state mới (this.state)

    //     let newState = { ...currentState, taskName: newProps.taskEdit.taskName }
    //     return newState;
    // }


    render() {
        return (
            <div>
                <ThemeProvider theme={this.props.themeToDoList}>
                    <Container className='w-50'>
                        <Dropdown onChange={(e) => {
                            let { value } = e.target;
                            //Dispatch value lên reducer 
                            this.props.dispatch(changeThemeAction(value))
                            this.props.dispatch({
                                type: 'change_theme',
                                themeId: value
                            })
                        }}>
                            {this.renderTheme()}
                        </Dropdown>
                        <Heading1 className='display-4'>To Do List </Heading1>

                        <TextField value={this.state.taskName} onChange={(e) => {
                            this.setState({
                                taskName: e.target.value
                            }, () => {
                                console.log(this.state);
                            })
                        }} name="taskName" label="task name" className="w-75"></TextField>


                        <Button onClick={() => {
                            // lấy thông tin người dùng nhập vào từ input
                            let { taskName } = this.state;
                            let newTask = {
                                id: Date.now(),
                                taskName: taskName,
                                done: false
                            }

                            console.log(newTask)
                            // tạo ra 1 task object
                            // đưa task object lên redux thông qua dispatch 

                            this.props.dispatch(addTaskAction(newTask))


                        }} className='ml-2' ><i className='fa fa-plus'></i> Add Task</Button>
                        {
                            this.state.disabled ? <Button disabled onClick={() => {
                                this.props.dispatch(updateTask(this.state.taskName))
                            }} className='ml-2' ><i className='fa fa-upload'></i> Update Task</Button> :

                                <Button onClick={() => {
                                    let { taskName } = this.state;
                                    this.setState({
                                        disabled: true,
                                        taskName: ''
                                    }, () => {
                                        this.props.dispatch(updateTask(taskName))
                                    })

                                }} className='ml-2' ><i className='fa fa-upload'></i> Update Task</Button>
                        }

                        {/* <Button disabled onClick={() => {
                            this.props.dispatch(updateTask(this.state.taskName))
                        }} className='ml-2' ><i className='fa fa-upload'></i> Update Task</Button> */}

                        <hr />
                        <Heading3>Nhiệm Vụ</Heading3>

                        <Table>
                            <Thead>
                                {this.renderTaskToDo()}
                            </Thead>
                        </Table>

                        <Heading3>Nhiệm Vụ Hoàn Thành </Heading3>

                        <Table>
                            <Thead>
                                {this.renderTaskCompleted()}
                            </Thead>
                        </Table>

                    </Container>
                </ThemeProvider>
            </div >
        )
    }

    // Đây là lifecycle trả về props cũ và state cũ của component trước khi render (lifecycle này chạy sau render)
    componentDidUpdate(prevProps, prevState) {
        // So sánh nếu như props trước đó (taskEdit trước mà khác taskEdit hiện tại thì mình với setState)
        if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
            this.setState({
                taskName: this.props.taskEdit.taskName
            })
        }
    }
}

const mapStateToProps = state => {
    return {
        themeToDoList: state.ToDoListReducer.themeToDoList,
        taskList: state.ToDoListReducer.taskList,
        taskEdit: state.ToDoListReducer.taskEdit
    }
}


export default connect(mapStateToProps)(ToDoList)